# E-Wallet

## Carrito de compras

Carrito de compras que utiliza un monedero de BitCoins, con opción de pago por PayPal. Entrega final del segundo cuatrimestre de la carrera de Diseño Web de Primera Escuela de Arte Multimedial Da Vinci.

- **Alumno**: Adrián Benavente
- **Cuatrimestre 2**
- **Comisión**: DWN2A
- **Turno**: Noche
