/* Resetear todos los filtros al hacer click en el logo */
$('#logo a').click(function (){
  $('.categoria > div').slideDown();
  $('.categoria > h2').removeClass('collapsed');
  $('[data-categoria="Computación"]').prependTo('.categorias');
  $('[data-categoria="Electrodomésticos"]').insertAfter('[data-categoria="Computación"]');
  $('[data-categoria="Telefonía"]').insertAfter('[data-categoria="Electrodomésticos"]');
});


/* Disparador de secciones */
$('#main-menu ul li a').click(function (e) {
  var targetSection = e.target.href.split('#')[1];
  $(`#main-menu ul li a`).removeClass('active');
  $(`#main-menu ul li a[href="#${targetSection}"]`).addClass('active');
  $(`section:not(#${targetSection}), #banner`).hide();
  $(`section#${targetSection}`).fadeIn();
  $('html, body').animate({
    scrollTop: $('body').offset().top
  }, 500);
});


/* Funcionalidad barra de filtros */
$('.filter-bar > ul > li > a').click(function () {
  $('section:not(#inicio)').hide();
  $('section#inicio').fadeIn();
  var targetFilter = $(this).data('target');
  $('.categoria').hide();
  $(`.categoria[data-categoria="${targetFilter}"]`).prependTo('.categorias');
  $('.categoria').fadeIn();
  $(`.categoria[data-categoria="${targetFilter}"] > div`).slideDown();
  $(`.categoria[data-categoria="${targetFilter}"] > h2`).removeClass('collapsed');
  $(`.categoria:not([data-categoria="${targetFilter}"]) > div`).hide();
  $(`.categoria:not([data-categoria="${targetFilter}"]) > h2`).addClass('collapsed');
});


/* Colapsar/expandir secciones */
$('.categoria h2').click(function() {
  $(this).siblings().slideToggle();
  $(this).toggleClass('collapsed');
});


/* Vaciar carrito */
var vaciarCarrito = function () {
  arrCarrito = [
    {
      'Computación': [],
      'Electrodomésticos': [],
      'Telefonía': []
    }
  ];

  contadorCarrito = 0;
  subtotal = 0;
  $('.cart-counter').removeClass('has-items');
  $('.subtotal').html('');

  for (var categoria in listaProductos[0]) {
    for (var cant of listaProductos[0][categoria]) {
      if (cant['Producto'].cantidad) {
        cant['Producto'].cantidad = null;
      }
    }
  }
}


/* Armar y mostrar ventana modal de prducto */
$('.categoria .card').click(function (e) {
  $('.modal-detalle-producto, .modal-cart').remove();
  var clickedProduct = listaProductos[0][e.currentTarget.parentElement.parentElement.dataset['categoria']][e.currentTarget.dataset['id']]['Producto'];

  var ventanaModal = new VentanaModal({
    title: clickedProduct.nombre,
    body: clickedProduct.desc,
    img: {
      desktop: {
        webp: clickedProduct.imagen.desktop.webp,
        jpg: clickedProduct.imagen.desktop.jpg
      },
      mobile: {
        webp: clickedProduct.imagen.mobile.webp,
        jpg: clickedProduct.imagen.mobile.jpg
      }
    },
    precio: clickedProduct.precio,
    _id_: clickedProduct._id_,
    categoria: e.currentTarget.parentElement.parentElement.dataset['categoria']
  });

  $('body').append(ventanaModal.setModal());
  ventanaModal.show();
});


/* Agregar item al carrito */
var contadorCarrito = 0;
var subtotal = 0;

var addToCart = function (e) {
  
  var carritoItem = {
    _id_: e.parentElement.parentElement.parentElement.nextSibling.dataset.idOrigen,
    categoria: e.parentElement.parentElement.parentElement.nextSibling.dataset.categoriaOrigen
  };
  
  var itemToAdd = listaProductos[0][carritoItem.categoria][carritoItem._id_];
  
  var arrCarritoCat = arrCarrito[0][carritoItem.categoria];

  var duplicated = $.inArray(itemToAdd, arrCarritoCat);
  
  if (duplicated === -1) {
    for (var cat of arrCarrito) {
      cat[carritoItem.categoria].push(itemToAdd);
    }
  } else {
    if (!arrCarritoCat[duplicated]['Producto'].cantidad) {
      arrCarritoCat[duplicated]['Producto'].cantidad = 2;
    } else {
      arrCarritoCat[duplicated]['Producto'].cantidad ++;
    }
  }
  
  subtotal += parseFloat(itemToAdd['Producto'].precio);

  contadorCarrito ++;

  $('.cart-counter').addClass('has-items');

  if (contadorCarrito > 99) {
    $('.cart-counter').html('99+');
  } else {
    $('.cart-counter').html(contadorCarrito);
  }

  $('.subtotal').html(`B: ${subtotal.toFixed(10)}`);
};


/* Quitar elemento del carrito */
var borrarItem = function (e) {
  var cat = e.target.dataset.refCat;
  var precio = e.target.dataset.refPrecio;
  var cantidad = e.target.dataset.refCantidad;

  subtotal -= parseFloat(precio);
  
  arrCarrito[0][cat].splice(arrCarrito[0][cat][0]['Producto'], 1);
  e.target.parentNode.removeChild(e.target.nextElementSibling);
  e.target.parentNode.removeChild(e.target);

  $('.subtotal').html(subtotal <= parseFloat(0) ? '' : `B: ${subtotal.toFixed(10)}`);

  $('.cart-total').html(subtotal <= parseFloat(0) ? 'Total: BTC 0.0000000000' : `Total: BTC ${subtotal.toFixed(10)}`);

  contadorCarrito -= cantidad;

  $('.cart-counter').html(contadorCarrito);

  if(contadorCarrito <= 0) {
    vaciarCarrito();
    $('.btn-vaciar').prop('disabled', true);
    $('.btn-checkout').prop('disabled', true);
  }
}


/* Ventana modal (carrito) */
var showCart = function () {
  $('.modal-detalle-producto, .modal-cart').remove();
  var ventanaModal = new ModalCarrito(arrCarrito[0]);

  $('body').append(ventanaModal.setModal());
  ventanaModal.show();
}

$('.cart-indicator').on('click', showCart);


/* Formulario de envío */
var setShipping = function () {
  var shippingForm = $('<form class="shipping-form"></form>');

  var formGroup = function () {
    var group = $('<div class="form-group"></div>');
    
    for (let child of arguments) {
      group.append(child);
    }

    shippingForm.append(group);
    return group;
  } 

  var errors = $(`<div class="form-errors"><span></span></div>`);

  var labelName = $('<label for="nombre">Ingresá tu nombre</label>');
  var inputName = $('<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ej: Andrés Díaz" autofocus>');
  var validName = false;
  $(inputName).on('input', (e) => {
    $('.form-errors span').html(validadorForm(e.target).message);
    validName = validadorForm(e.target).valid;
  });
  
  
  var labelEmail = $('<label for="email">Ingresá tu email</label>');
  var inputEmail = $('<input type="email" class="form-control" name="email" id="email" placeholder="Ej: alediaz@mail.com">');
  var validEmail = false;
  $(inputEmail).on('input', (e) => {
    $('.form-errors span').html(validadorForm(e.target).message);
    e.target.setAttribute('data-valid', validadorForm(e.target).valid);
    validEmail = validadorForm(e.target).valid;
  });
  
  
  var labelDirEnvio = $('<label for="envio">¿A dónde te enviamos tu compra?</label>');
  var inputDirEnvio = $('<input type="text" class="form-control" name="envio" id="envio" placeholder="Ej: Viamonte 865">');
  var validDirEnvio = false;
  $(inputDirEnvio).on('input', (e) => {
    $('.form-errors span').html(validadorForm(e.target).message);
    e.target.setAttribute('data-valid', validadorForm(e.target).valid);
    validDirEnvio = validadorForm(e.target).valid;
  });
  
  
  formGroup(labelName, inputName);
  formGroup(labelEmail, inputEmail);
  formGroup(labelDirEnvio, inputDirEnvio);
  
  shippingForm.append(errors);
  $(shippingForm).on('input', () => {
    if(validName && validEmail && validDirEnvio) {
      $('.btn-pagar').prop('disabled', false);
    } else {
      $('.btn-pagar').prop('disabled', true);
    }
  })

  return shippingForm;
}


/* Ingreso de datos de envío */
var getShipping = function (e) {
  var btnPagar = e.target.cloneNode(true);
  btnPagar.innerHTML = 'Pagar';
  btnPagar.disabled = 'disabled';
  btnPagar.classList.add('btn-pagar');

  var modalFooter = e.target.parentElement;
  $(modalFooter).append('<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>', btnPagar);
  
  e.target.style.display = 'none'; // Botón "¡Comprá ya!" (donde el usuario hizo click)
  e.target.previousElementSibling.style.display = 'none'; // Botón "Vaciar carrito"
  
  $(btnPagar).click(getCheckout);
  
  $('.cart-items-list').hide();
  $('.cart-total').hide();
  
  var modalBody = e.target.parentElement.previousElementSibling;

  setTimeout(() => {
    $(modalBody).append(setShipping());
    $(modalBody).fadeIn();
  }, 500);
}


/* Checkout */
var setCheckout = function () {
  this.total = subtotal;
  var checkout = $('<div class="checkout-step"></div>');
  var title = $('<h2>¿Cómo deseás pagar?</h2>');
  var wallet = $(`
    <div class="my-wallet">
      <h3>Mi E-Wallet</h3>
      <span class="saldo">BTC ${saldo.toFixed(10)}</span>
    </div>
  `);

  checkout.append(title);
  checkout.append(wallet);

  /* Botón para pagar con saldo en BitCoins (E-Wallet) */
  var btnComprar = $('<button type="button" class="finalizar-compra" data-dismiss="modal">Pagar</button>');
  $(btnComprar).on('click', function () {
    comprar('e-wallet');
  });

  if (saldo < this.total) {
    checkout.append(`<span class="limit-reached">No tenés saldo suficiente para realizar esta compra</span>`);
  } else {
    checkout.append(`<span class="saldo-despues">Después: BTC ${(saldo - this.total).toFixed(10)}</span>`);
    checkout.append(btnComprar);
  }

  /* Botón comprar con PayPal */
  var btnPaypal = $(`<button type="button" class="paypal-btn" data-dismiss="modal">PayPal</button>`);
  $(btnPaypal).on('click', function () {
    comprar('paypal');
  });
  checkout.append(btnPaypal);

  return checkout;
}

var getCheckout = function () {
  if (arrCarrito.length) {
    $('.modal-footer').hide();
    $('.shipping-form').hide();
    $(setCheckout()).prependTo('.modal-cart .modal-body');
  }
}


/* Pagar y finalizar la compra */
var comprar = function(formaDePago) {
  if (formaDePago == 'paypal') {
    window.open('https://paypal.com', '_blank');
  }

  if (formaDePago == 'e-wallet') {
    saldo -= total;
  }

  var today = new Date();

  var arrives = `${today.getDate()+10 > 30 ? (today.getDate()+10) - 30 : today.getDate()+10}-${today.getDate()+10 > 30 ? today.getMonth()+2 : today.getMonth()+1}-${today.getMonth()+1 > 12 ? today.getFullYear()+1 : today.getFullYear()}`;

  for (let categoria in arrCarrito[0]) {
    for (let producto in arrCarrito[0][categoria]) {
      arrCarrito[0][categoria][producto]['Producto'].fecha = arrives;
    }
  }

  arrPedidos[contPedidos] = arrCarrito;
  contPedidos++;

  mostrarPedidos();

  /* Vaciar carrito de compras */
  vaciarCarrito();

  $('#saldo').html(saldo.toFixed(10));

  $('html, body').animate({
    scrollTop: $('body').offset().top
  }, 500);

  var success = $(`
    <div class="container-fluid">
      <div class="compra-exitosa alert alert-success alert-dismissible" role="alert">
        <strong>¡Gracias por tu compra!</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>
  `);

  $(success).insertBefore('#banner');

  $(success).delay(3000).fadeOut();
}


/* Explorar categorías */

$('#explore').click(() => {
  $('#mispedidos').hide();
  $('#inicio').fadeIn();
  $('a[href="#mispedidos"]').removeClass('active');
  $('a[href="#inicio"]').addClass('active');
});


/* Cargar un producto */

document.querySelector('#venderalgo form').addEventListener('submit', (e) => {
  var productForm = e.target;
  var productName = document.querySelector('#nombreProducto').value;
  var productPrice = document.querySelector('#precioProducto').value;
  var productDesc = document.querySelector('#descProducto').value;
  var fileInputElement = document.querySelector('#venderalgo form input[type=file]');
  var fileName = fileInputElement.files[0].name;
  
  var formData = new FormData();

  formData.append('fotoProducto', fileInputElement.files[0]);

  var request = new XMLHttpRequest();
  request.open('POST', 'upload.php');
  request.send(formData);

  request.onloadend = () => {
    $('#venderalgo .publicaciones').show();
    $('#venderalgo .publicaciones').append(`
      <div class="publicado">
        <div>
          <h3>${productName}</h3>
          <span>BTC ${(0.0000021639 * parseFloat(productPrice)).toFixed(10)}</span>
          <p>${productDesc}</p>
        </div>
        <div>
          <img class="img-fluid" src="assets/img/${fileName}" alt="">
        </div>
      </div>
    `);

    productForm.reset();
  }

  e.preventDefault();
});


/* Contacto */

document.querySelector('#contacto form').addEventListener('submit', (e) => {
  var personaForm = e.target;
  
  var formData = new FormData(personaForm);
  
  var request = new XMLHttpRequest();
  request.open("POST", "send.php");
  request.send(formData);

  request.onloadend = () => {
    $('#contacto .output').append(`
      <div class="alert alert-success alert-dismissible" role="alert">
        <strong>¡Gracias por contactarnos!</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    `);

    personaForm.reset();
  }

  e.preventDefault();
});