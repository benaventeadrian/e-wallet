/* Saldo disponible del usuario para gastar */
var saldo = 1.0000000000;

var validadorForm = function (field) {
  var re = '';
  var error = '';

  if (field.value == '') {
    error = `El campo ${field.name} no puede estar vacío`;
    
    return {
      valid: false,
      message: error
    }
  } else if (field.name == 'nombre') {
    re = /^[a-zA-ZáéíóúÁÉÍÓÚñN\s]+$/;
    error = re.test(field.value) ? '' :  'Tu nombre solo puede contener letras';
  } else if (field.name == 'email') {
    re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    error = re.test(field.value) ? '' :  'Tu email tiene un formato inválido';
  } else {
    re = /^[a-zA-Z0-9áéíóúÁÉÍÓÚñN\s\d]+$/;
    error = re.test(field.value) ? '' : 'Por favor ingresá solo letras y números';
  }

  return {
    valid: re.test(field.value),
    message: error
  }
}

/* Componente barra de filtros */
var FilterBar = function (categorias) {
  this.categorias = categorias;
}

FilterBar.prototype.setFilterBar = function () {
  var filtros = $('<div class="filter-bar"></div>');
  var filtrosUl = $('<ul class="container"></ul>');

  for (var categoria in this.categorias) {
    filtrosUl.append(`<li><a href="#" data-target="${categoria}">${categoria}</a></li>`)
  }

  filtros.append(filtrosUl);

  return filtros;
}

/* Componente producto */
var Producto = function (data) {
  this._id_ = data._id_;
  this.categoria = data.categoria || 'Sin categoría';
  this.nombre = data.nombre || 'No se especifica';
  this.desc   = data.desc   || 'No se especifica';
  this.precio = (0.0000021639 * data.precio.valor).toFixed(10) || 'No se especifica';
  this.imagen = data.imagen || 'assets/img/mascot-error.svg';
}

Producto.prototype.setProducto = function () {
  var prodCard = $(`<div class="card producto ${this._id_ == 0 ? 'destacado' : ''}" data-id="${this._id_}"></div>`);

  var prodImg = $('<div class="card-img"></div>');

  prodImg.append(`
    <picture>
      <source type="image/webp" srcset="${this.imagen.desktop.webp}" media="(min-width: 920px)">
      <source type="image/jpeg" srcset="${this.imagen.desktop.jpg}" media="(min-width: 920px)">
      <source type="image/webp" srcset="${this.imagen.mobile.webp}">
      <img class="card-img-top img-fluid" src="${this.imagen.mobile.jpg}" alt="${this.nombre}">
    </picture>
  `);

  
  var prodBody = $('<div class="card-body"></div>');
      prodBody.append(`<h3 class="card-title">${this.nombre}</h3>`);
      prodBody.append(`<span class="card-price">BTC ${this.precio}</span>`);
      prodBody.append(`<p class="card-text">${this.desc}</p>`);
      prodBody.append('<button class="btn btn-primary btn-comprar">Ver más</button>');

      prodCard.append(prodImg, prodBody);

  return prodCard;
}

/* Componente ventana modal */
var VentanaModal = function(contents) {
  this.title = contents.title || 'Sin título';
  this.img = contents.img || '../assets/img/mascot-error.svg';
  this.body = contents.body || 'Nada por aquí';
  this.precio = contents.precio || 'No especificado';
  this._id_ = contents._id_;
  this.categoria = contents.categoria;
}

VentanaModal.prototype.setModal = function() {
  var modal = $('<div class="modal modal-detalle-producto" role="dialog"></div>');
  var modalDialog = $('<div class="modal-dialog modal-dialog-centered" role="document"></div>');
  var modalContent = $('<div class="modal-content"></div>');
  
  var modalHeader = $('<div class="modal-header"></div>');
      modalHeader.append(`<h2 class="modal-title">${this.title}</h2>`);
      modalHeader.append(`
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      `);

  var modalBody = $('<div class="modal-body"></div>');

  var modalDismiss = $('<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>');
  var modalAddToCart = $('<button type="button" class="btn btn-primary btn-comprar add-to-cart" data-dismiss="modal">Añadir al carrito</button>');
      $(modalAddToCart).on('click', function () {
        addToCart(this);
      });
  var modalFooter = $('<div class="modal-footer"></div>');
      modalFooter.append(modalDismiss, modalAddToCart);
  
      modalBody.append(`
        <picture>
          <source type="image/webp" srcset="${this.img.desktop.webp}" media="(min-width: 577px)">
          <source type="image/jpeg" srcset="${this.img.desktop.jpg}" media="(min-width: 577px)">
          <source type="image/webp" srcset="${this.img.mobile.webp}" media="(max-width: 576px)">
          <img class="card-img-top img-fluid" src="${this.img.mobile.jpg}" alt="${this.title}">
        </picture>
        <p>${this.body}</p>
        <h3>BTC ${this.precio}*</h3>
        <small>*Valores expresados en BitCoins</small>
      `);

      modalContent.append(modalHeader, modalBody, modalFooter);
      modalDialog.append(modalContent);

      modal.append(modalDialog);
      modal.append(`<div class="modal-metadata" aria-hidden="true" data-categoria-origen="${this.categoria}" data-id-origen="${this._id_}"></div>`);

  return modal;
}

VentanaModal.prototype.show = function() {
  $('.modal-detalle-producto').modal('show');
}

/* Ventana modal de carrito de compras */
var ModalCarrito = function (items) {
  this.title = 'Carrito de compras';
  this.items = items;
}

ModalCarrito.prototype.setModal = function () {
  var modal = $('<div class="modal modal-cart" role="dialog"></div>');
  var modalDialog = $('<div class="modal-dialog modal-dialog-centered"" role="document"></div>');
  var modalContent = $('<div class="modal-content"></div>');
  var modalHeader = $('<div class="modal-header"></div>');
      modalHeader.append(`<h2 class="modal-title">${this.title}</h2>`);
      modalHeader.append(`
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      `);
  
  var modalBody = $('<div class="modal-body"></div>');

  var checkEmptyCart = false;

  if (arrCarrito[0]['Computación'].length == 0 && arrCarrito[0]['Electrodomésticos'].length == 0 && arrCarrito[0]['Telefonía'].length == 0) {
    checkEmptyCart = true;
  }
  
   /* Botón vaciar carrito */
  var botonVaciar = $(`<button type="button" class="btn btn-secondary btn-vaciar" data-dismiss="modal" ${checkEmptyCart ? 'disabled' : '' }>Vaciar carrito</button>`);
  $(botonVaciar).on('click', vaciarCarrito);

   /* Botón realizar checkout */
  var botonComprar = $(`<button type="button" class="btn btn-primary btn-comprar btn-checkout" ${checkEmptyCart ? 'disabled' : '' }>¡Compralo ya!</button> `);
  $(botonComprar).on('click', getShipping);

  var modalFooter = $('<div class="modal-footer"></div>');
      modalFooter.append(botonVaciar, botonComprar);


  var itemsList = $('<div class="cart-items-list"></div>');
  var itemRow, itemInfo, precioTotal = 0;

  var generarLista = function (item) {
    itemRow = $(`<div class="cart-item-row d-flex"></div>`);
    itemInfo = $('<div class="cart-item-info d-flex flex-column"></div>');

    itemInfo.append(`<h3>${item.nombre}</h3>`);
    itemInfo.append(`<div><span>Cantidad: ${(item.cantidad ? item.cantidad : 1 )} </span></div>`);
    itemInfo.append(`<div><span>BTC: ${(item.precio * (item.cantidad ? item.cantidad : 1 )).toFixed(10)}</span></div>`);
    
    itemRow.append(itemInfo);
    itemRow.append(`
      <div class="cart-item-img">
        <picture>
          <source type="image/webp" srcset="${item.imagen.desktop.webp}" media="(min-width: 577px)">
          <source type="image/webp" srcset="${item.imagen.desktop.webp}" media="(min-width: 577px)">
          <source type="image/webp" srcset="${item.imagen.mobile.webp}">
          <img class="img-fluid" src="${item.imagen.mobile.jpg}" alt="${item.nombre}">
        </picture>
      </div>
    `);

    itemsList.append(itemRow);

    var btnCerrar = $(`<button class="btn-borrar-item" data-ref-id="${item._id_}" data-ref-cat="${item.categoria}" data-ref-precio="${item.precio * (item.cantidad ? item.cantidad : 1 )}" data-ref-cantidad="${item.cantidad ? item.cantidad : 1}">&times;</button>`);
    $(btnCerrar).on('click', borrarItem);

    btnCerrar.insertBefore(itemRow);

    precioTotal += (item.precio * (item.cantidad ? item.cantidad : 1 ));
  }
  
  for (var categoria in this.items) {
    for (var item of this.items[categoria]) {
      generarLista(item['Producto']);
    }
  }
  
    modalBody.append(itemsList);
    modalBody.append(`<span class="cart-total">Total: BTC ${precioTotal.toFixed(10)}</span>`);
    modalContent.append(modalHeader, modalBody, modalFooter);
    modalDialog.append(modalContent);

    modal.append(modalDialog);

  return modal;
}

ModalCarrito.prototype.show = function () {
  $('.modal-cart').modal('show');
}

var mostrarPedidos = function () {  
  $('#mispedidos .pedidos').html('');

  for (let i = 0; i < arrPedidos.length; i++) {
    for (let categoria in arrPedidos[i][0]) {
      for (var producto in arrPedidos[i][0][categoria]) {
        $('#mispedidos .pedidos').append(`
        <div class="pedido">
          <div>
            <h3>${arrPedidos[i][0][categoria][producto]['Producto'].nombre}</h3>
            <h4>Llega el ${arrPedidos[i][0][categoria][producto]['Producto'].fecha}</h4>
          </div>
          <div>
            <picture>
              <source srcset="${arrPedidos[i][0][categoria][producto]['Producto'].imagen.mobile.webp}" type="image/webp">
              <img class="img-fluid" src="${arrPedidos[i][0][categoria][producto]['Producto'].imagen.mobile.jpg}" alt='${arrPedidos[i][0][categoria][producto]['Producto'].desc}'>
            </picture>
          </div>
        </div>
      `);
      }
    }
  }

  for (let i = 0; i < arrPedidos.length; i++) {
    for (let categoria in arrPedidos[i][0]) {
      for (var producto in arrPedidos[i][0][categoria]) {
        c.log(arrPedidos[i][0][categoria][producto]['Producto']);
      }
    }
  }
}