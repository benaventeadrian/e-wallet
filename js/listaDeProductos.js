var listaProductos = [
  {
    'Computación': [
      {
        'Producto': new Producto({
          _id_: 0,
          categoria: 'Computación',
          nombre: 'Raspberry Pi 3',
          desc: 'KIT STEM Raspberry Pi 3 Pantalla 3.5" con memoria de 32GB. Listo para armar tu proyecto. Incluye drivers y sistema operativo instalado.',
          precio: {
            valor: 4169,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/computacion/raspberry-pi-3-desktop.webp',
              jpg: 'assets/img/computacion/raspberry-pi-3-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/computacion/raspberry-pi-3-mobile.webp',
              jpg: 'assets/img/computacion/raspberry-pi-3-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 1,
          categoria: 'Computación',
          nombre: 'Disco SSD Kingston 480gb',
          desc: 'Disco Solido 480gb Kingston A400 Ssd 550mbps 2.5 Full Mexx',
          precio: {
            valor: 3199,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/computacion/disco-kingston-ssd-480gb-desktop.webp',
              jpg: 'assets/img/computacion/disco-kingston-ssd-480gb-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/computacion/disco-kingston-ssd-480gb-mobile.webp',
              jpg: 'assets/img/computacion/disco-kingston-ssd-480gb-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 2,
          categoria: 'Computación',
          nombre: 'Huawei Matebook X Pro',
          desc: 'Huawei Matebook X Pro I7-8550u 16gb 512gb Ssd En Stock',
          precio: {
            valor: 99990,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/computacion/huawei-matebook-x-pro-desktop.webp',
              jpg: 'assets/img/computacion/huawei-matebook-x-pro-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/computacion/huawei-matebook-x-pro-desktop.webp',
              jpg: 'assets/img/computacion/huawei-matebook-x-pro-desktop.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 3,
          categoria: 'Computación',
          nombre: 'Notebook Lenovo Thinkpad',
          desc: 'Notebook Lenovo Thinkpad X1 Carbon Core I7 8gb 256gb Win10',
          precio: {
            valor: 107480,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/computacion/notebook-lenovo-thinkpad-x1-carbon-desktop.webp',
              jpg: 'assets/img/computacion/notebook-lenovo-thinkpad-x1-carbon-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/computacion/notebook-lenovo-thinkpad-x1-carbon-mobile.webp',
              jpg: 'assets/img/computacion/notebook-lenovo-thinkpad-x1-carbon-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 4,
          categoria: 'Computación',
          nombre: 'Memoria Sodimm 8gb Kingston',
          desc: 'Memoria Sodimm 8gb Kingston Ddr3 1600mhz Notebook Mexx',
          precio: {
            valor: 2499,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/computacion/memoria-sodimm-8gb-kingston-desktop.webp',
              jpg: 'assets/img/computacion/memoria-sodimm-8gb-kingston-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/computacion/memoria-sodimm-8gb-kingston-mobile.webp',
              jpg: 'assets/img/computacion/memoria-sodimm-8gb-kingston-mobile.jpg'
            }
          }
        })
      },
    ],
    'Electrodomésticos': [
      {
        'Producto': new Producto({
          _id_: 0,
          categoria: 'Electrodomésticos',
          nombre: 'Xiaomi AQara',
          desc: 'Original Xiaomi AQara WXKG12LM interruptor inteligente inalámbrico aplicación inteligente Control remoto/timbre para dispositivos inteligentes Xiaomi mi hogar',
          precio: {
            valor: 510,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/electro/xiaomi-aquara-desktop.webp',
              jpg: 'assets/img/electro/xiaomi-aquara-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/electro/xiaomi-aquara-mobile.webp',
              jpg: 'assets/img/electro/xiaomi-aquara-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 1,
          categoria: 'Electrodomésticos',
          nombre: 'Harman Kardon Soundsticks 3',
          desc: 'Harman kardon SoundSticks III Cuatro transductores de rango completo de 1 "por canal alimentados por 10 vatios',
          precio: {
            valor: 25000,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/electro/harman-kardon-soundsticks-3-desktop.webp',
              jpg: 'assets/img/electro/harman-kardon-soundsticks-3-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/electro/harman-kardon-soundsticks-3-mobile.webp',
              jpg: 'assets/img/electro/harman-kardon-soundsticks-3-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 2,
          categoria: 'Electrodomésticos',
          nombre: 'Cafetera Express Philips Saeco',
          desc: 'Cafetera Express Philips Saeco Hd8323/42 1litro 15 Bares',
          precio: {
            valor: 7699,
            moneda: 'ARS'
          }, 
          imagen: {
            desktop: {
              webp: 'assets/img/electro/cafetera-express-philips-saeco-desktop.webp',
              jpg: 'assets/img/electro/cafetera-express-philips-saeco-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/electro/cafetera-express-philips-saeco-mobile.webp',
              jpg: 'assets/img/electro/cafetera-express-philips-saeco-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 3,
          categoria: 'Electrodomésticos',
          nombre: 'Aspiradora Robot',
          desc: 'Aspiradora Robot Compacta Potente Sin Bolsa Inalambrica',
          precio: {
            valor: 12000,
            moneda: 'ARS'
          }, 
          imagen: {
            desktop: {
              webp: 'assets/img/electro/aspiradora-robot-desktop.webp',
              jpg: 'assets/img/electro/aspiradora-robot-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/electro/aspiradora-robot-mobile.webp',
              jpg: 'assets/img/electro/aspiradora-robot-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 4,
          categoria: 'Electrodomésticos',
          nombre: 'Lampara Led Smart Foco E27',
          desc: 'Lampara Led Smart Foco E27 Rgb Multicolor Wi Fi.Control de hasta 200 lámparas en simultáneo. Gran potencia! (Equivalente 20W de las lamparas tradicionales).',
          precio: {
            valor: 1149,
            moneda: 'ARS'
          }, 
          imagen: {
            desktop: {
              webp: 'assets/img/electro/lampara-led-smart-foco-desktop.webp',
              jpg: 'assets/img/electro/lampara-led-smart-foco-desktop.jpg'
              
            },
            mobile: {
              webp: 'assets/img/electro/lampara-led-smart-foco-mobile.webp',
              jpg: 'assets/img/electro/lampara-led-smart-foco-mobile.jpg'

            }
          }
        })
      },
    ],
    'Telefonía': [
      {
        'Producto': new Producto({
          _id_: 0,
          categoria: 'Telefonía',
          nombre: 'Xiaomi Redmi Note 7',
          desc: 'Xiaomi Redmi Note 7 Dual SIM 64 GB Dream blue',
          precio: {
            valor: 13200,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/telefonia/xiaomi-redmi_7-desktop.webp',
              jpg: 'assets/img/telefonia/xiaomi-redmi_7-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/telefonia/xiaomi-redmi_7-mobile.webp',
              jpg: 'assets/img/telefonia/xiaomi-redmi_7-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 1,
          categoria: 'Telefonía',
          nombre: 'Motorola Z3 Play',
          desc: 'Motorola Z3 Play 64 GB Índigo oscuro',
          precio: {
            valor: 18000,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/telefonia/moto-z3_play-desktop.webp',
              jpg: 'assets/img/telefonia/moto-z3_play-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/telefonia/moto-z3_play-mobile.webp',
              jpg: 'assets/img/telefonia/moto-z3_play-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 2,
          categoria: 'Telefonía',
          nombre: 'One Plus 7',
          desc: 'Global Rom OnePlus 7 8 GB RAM 256GB ROM Smartphone Snapdragon 855 de 6,41 pulgadas Pantalla AMOLED huella 48MP cámaras UFS 3,0',
          precio: {
            valor: 35000,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/telefonia/one-plus-7-desktop.webp',
              jpg: 'assets/img/telefonia/one-plus-7-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/telefonia/one-plus-7-mobile.webp',
              jpg: 'assets/img/telefonia/one-plus-7-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 3,
          categoria: 'Telefonía',
          nombre: 'Samsung Galaxy Note9',
          desc: 'Samsung Galaxy Note9 Dual SIM 128 GB Midnight Black',
          precio: {
            valor: 51600,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/telefonia/samsung-galaxy-note-9-desktop.webp',
              jpg: 'assets/img/telefonia/samsung-galaxy-note-9-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/telefonia/samsung-galaxy-note-9-mobile.webp',
              jpg: 'assets/img/telefonia/samsung-galaxy-note-9-mobile.jpg'
            }
          }
        })
      },
      {
        'Producto': new Producto({
          _id_: 4,
          categoria: 'Telefonía',
          nombre: 'Telefono Celular Lg G7',
          desc: 'Telefono Celular Lg G7 4g Snapdragon Dual Cam Octa 4gb Nfc',
          precio: {
            valor: 33500,
            moneda: 'ARS'
          },
          imagen: {
            desktop: {
              webp: 'assets/img/telefonia/telefono-celular-lg-g7-desktop.webp',
              jpg: 'assets/img/telefonia/telefono-celular-lg-g7-desktop.jpg'
            },
            mobile: {
              webp: 'assets/img/telefonia/telefono-celular-lg-g7-mobile.webp',
              jpg: 'assets/img/telefonia/telefono-celular-lg-g7-mobile.jpg'
            }
          }
        })
      },
    ]
  }
];