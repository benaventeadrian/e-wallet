var c = console;
var arrCarrito = [
  {
    'Computación': [],
    'Electrodomésticos': [],
    'Telefonía': []
  }
];

var arrPedidos = [];
var contPedidos = 0;

var filterBar = new FilterBar(listaProductos[0]);


/* Agrego barra superior de navegación */
$(filterBar.setFilterBar()).insertAfter('.top-bar > div');

$('body').css({
  'padding-top': $('.top-bar').height() + 20,
  'padding-bottom': $('#main-menu').height() + 30
});

$(window).resize(function() {
  $('body').css({
    'padding-top': $('.top-bar').height() + 20,
    'padding-top': $('#main-menu').height() + 30
  });
});


/* Inicio (categorías) */

for (var categoria in listaProductos[0]) {
  $('.categorias').append(`
    <div class="row categoria" data-categoria="${categoria}">
      <h2 class="col-12">${categoria}</h2>
      <div class="products-grid-wrapper"></div>
    </div>
  `);
}

for (var prod of listaProductos[0]['Computación']) {
  $('.categoria:first-of-type > .products-grid-wrapper').append(prod['Producto'].setProducto());
}

for (var prod of listaProductos[0]['Electrodomésticos']) {
  $('.categoria:nth-of-type(2) > .products-grid-wrapper').append(prod['Producto'].setProducto());
}

for (var prod of listaProductos[0]['Telefonía']) {
  $('.categoria:last-of-type > .products-grid-wrapper').append(prod['Producto'].setProducto());
}


/* Mi E-Wallet */
$('#saldo').html(`BTC: ${saldo.toFixed(10)}`);