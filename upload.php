<?php
$target_dir = "assets/img/";
$target_file = $target_dir . basename($_FILES["fotoProducto"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fotoProducto"]["tmp_name"]);
    if($check !== false) {
        echo "El archivo es una imagen - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "El archivo no es una imagen.";
        $uploadOk = 0;
    }
}

if ($uploadOk == 0) {
  echo "No se pudo cargar tu archivo".basename($_FILES["fotoProducto"]["name"]).", volvé a intentarlo más tarde.";
} else {
  if (move_uploaded_file($_FILES["fotoProducto"]["tmp_name"], $target_file)) {
      echo "El archivo ". basename($_FILES["fotoProducto"]["name"]). " ha sido cargado exitosamente.";
  } else {
      echo "No se pudo cargar tu archivo ".$target_file." desde ".$_FILES["fotoProducto"]["tmp_name"].", volvé a intentarlo más tarde.";
  }
}
?>